﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinPropellerX : MonoBehaviour
{

    public float speed;
    private Vector3 revolution;

    // Start is called before the first frame update
    void Start()
    {
        revolution = new Vector3(0, 0, -1);
        speed = 1000;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(revolution * speed * Time.deltaTime);
    }
}
